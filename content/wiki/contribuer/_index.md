+++
title = "Contribuer"
date = 2017-03-27
aliases = [ "contribuer__old",]
weight = 7

[taxonomies]
authors = [ "cgeek", ]
tags = [ "blockchain", "TRM", "yunohost",]
+++

# Contribuer

Selon que vous êtes totalement nouveau dans le monde de Ğ1, déjà membre, postulant, utilsateur de la monnaie ou informaticien (liste non exhaustive), vous trouverez dans tous les cas d'excellentes choses à réaliser pour faire vivre cette monnaie et contribuer à son partage et son utilisation avec d'autres humains.

Voici quelques possibilités.

#### Vous êtes totalement nouveau

Suivez quelques tutoriels pour [tenter de devenir membre de la Toile de Confiance Ğ1](@/wiki/g1/licence-g1.md). C'est la toute 1ère étape : devenir utilisateur !

#### Vous avez déjà utilisé un peu Ğ1

Vous avez certainement appris beaucoup de choses : partagez-les ! Par exemple vous pouvez relater votre expérience, vos difficultés et vos solutions sur votre propre blog, sur [forum.monnaie-libre.fr](https://forum.monnaie-libre.fr) ou [ici même](@/wiki/contribuer/ameliorer-le-site.md) !

#### Vous êtres membre de Ğ1

Invitez de nouvelles personnes ! Si vous les connaissez bien et qu'elles souhaitent devenir membre, aidez-les à s'inscrire puis donnez-leur votre certification !

La meilleure façon de faire prendre de la valeur à Ğ1 est de la faire adopter par le plus grande nombre.

#### Vous êtes informaticien

**Si vous êtes développeur**, vous avez l'embarras du choix : il est possible de participer au développement du cœur (Node.js), ou encore des clients tels Sakia (Python) / Cesium (HTML/CSS/JS) / Silkaj (Python), ou même de développer votre propre logiciel reposant sur Ğ1 et Duniter. Par exemple, il n'existe pas encore de marché pour réaliser des échanges : ce serait pourtant un formidable outil pour immédiatement donner tout son intérêt à la monnaie Ğ1 : faire des échanges !

**Si vous n'êtes pas développeur**, il vous reste des qualités ! Notamment votre œil d'informaticien saura repérer les problématiques de sécurité, d'installation ou encore d'ergonomie des logiciels ! Faites donc profitez l'ensemble de la communauté de vos propres compétences et de votre énergie.

Dans tous les cas, vous pouvez aller faire un tour sur le [Wiki](@/wiki/_index.md) afin de réperer les sujets pouvant vous intéresser.

### Vous avez trouvé un bug !

N'hésitez surtout pas à le rapporter ! Cela permet aux logiciels d'évoluer et évitera à d'autres de tomber dessus.

Instructions sur la page [Wiki > Rapporter un bug](@/wiki/contribuer/rapporter-un-bug.md).

### Financer les développments

Si vous disposez de quelques Ğ1, vous pouvez **[faire un don](@/FAQ/financements.md)** aux développeurs.

Et si vous n'en avez pas, vous pouvez aussi [en obtenir](@/FAQ/obtenir-des-g1.md).

