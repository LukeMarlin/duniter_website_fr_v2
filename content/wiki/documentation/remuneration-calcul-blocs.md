+++
aliases = [ "remuneration-calcul-blocs",]
date = 2017-04-10
weight = 5
title = "Être rémunéré en calculant des blocs"

[taxonomies]
authors = [ "inso",]
+++

# Comment être rémunéré en calculant des blocs

## Un protocole neutre

Dans Duniter, à la différence des cryptomonnaies usuelles, le réseau n'intègre pas de *taxe* sur les transactions.

La raison est qu'il faut éviter de mélanger les sujets. 

Pour qu'elle reste simple, la base technique (la blockchain, le réseau P2P) 
ne devrait pas intégrer les moyens de rémunérer ceux qui hébergent les noeuds.

À terme, l'économie Duniter se développera par elle-même et inventera ses solutions **par-dessus** le calcul des blocs. 
Par exemple, on peut envisager une société qui accepterait de ne calculer des blocs contenant des transactions 
d'utilisateurs que s'ils ont envoyé de la monnaie à cette société. 
Elle calculerait alors des blocs contenant la transaction de l'utilisateur ainsi que le paiement requis pour la transaction.

Il faut noter que cet élément **économique** se serait alors bien effectué **par-dessus le protocole technique**. 
Ce dernier est neutre, et n'induit par défaut aucun comportement particulier vis-à-vis de l'activité de calcul des blocs.

## Remuniter : le financement participatif du calcul

Aujourd'hui, le calcul des blocs peut être rémunéré par le service [Remuniter](https://remuniter.cgeek.fr/#/).

Le fonctionnement est simple : 

Les utilisateurs de la monnaie libre peuvent faire un don sur la clé de Remuniter : 

```
TENGx7WtzFsTXwnbrPEvb6odX2WnqYcnnrjiiLvp1mS
```

et le service va se charger de distribuer aux calculateurs la monnaie ainsi récoltée.
 
![RemuniterScreenshot](/PELICAN/images/wiki/remuniter.png)

