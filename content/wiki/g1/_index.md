+++
title = "Monnaie ğ1"
weight = 3
+++

La monnaie ğ1 (prononcée "june") est la première monnaie créée à l'aide du logiciel duniter.


- [Page de présentation](@/g1.md)
- [Comment obtenir de Ğ1 ?](@/FAQ/obtenir-des-g1.md)
- [FAQ](@/FAQ/faq-g1.md)
- [Licence ǧ1](@/wiki/g1/licence-g1.md)