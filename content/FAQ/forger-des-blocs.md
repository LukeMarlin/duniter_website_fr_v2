+++
title = "Miner des blocs ?"
weight = 1

aliases = ["fr/miner-des-blocs"]
+++

## Miner des blocs ? Non : les forger !

Dans une monnaie libre, on ne peut pas "miner" des blocs.

Le terme "minage" renvoie en effet à la création monétaire qui est attribuée, dans certaines monnaies non libres, à ceux qui participent au calcul des blocs.

Ce mécanisme reproduit les systèmes monétaires basés sur les métaux précieux comme l'or.

Or, par définition, l'or ne peux pas être une monnaie libre (et ne peut d'ailleurs jamais rester très longtemps une monnaie tout court, du fait de sa rareté qui le rend facilement déflationniste).

## Pourquoi forger des blocs ?

Pour la même raison que vous partagez des fichiers torrents sans attendre de rémunération en retour : simplement
pour faire vivre le réseau. 

Il y a plusieurs facteurs à prendre en compte :

1. **Seuls les noeuds associés à un compte membre peuvent calculer les blocs.**
2. **Le calcul de bloc dans Duniter ne coûte presque rien.** Ce qui sécurise la blockchain est le fait que chaque calculateur
est explicitement associé à un être humain. Ainsi, pas de risque d'attaque Sybil, et il n'y a aucune course à la
puissance nécessaire ! Tant que votre PC est dans la moyenne des puissances existantes (environ du Raspberry Pi au PC 4 cœurs), votre nœud pourra participer au calcul des blocs.
3. **Vous ne devriez pas attendre que d'autres membres calculent pour vous.** Exécuter votre nœud, c'est vous assurer que le 
réseau respecte les règles communes inscrites dans la blockchain. Ce nœud s'assurera pour vous que les Dividendes
Universels sont correctements générés, et que les transactions sont transmises.
