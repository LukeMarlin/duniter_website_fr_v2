+++
title = "FAQ"
weight = 3


# TODO unifier les faqs monnaie libre / Ğ1 / Duniter
# (l'idée d'une FAQ est que justement on ne sait pas forcément différencier les trois)
# réfléchir à l'intégration de https://borispaing.fr/doc/!monnaie/faq-index
# TODO template spécifique à la FAQ avec des questions dépliantes

+++

# FAQ

Foire aux questions, ou *frequently asked questions*, comme vous l'entendez. Cette section destinée aux néophytes répond aux questions pratiques sur le fonctionnement et l'utilisation de la monnaie libre, de la ğ1 et de duniter. Pour des question plus avancées, veuillez plutôt consulter la section [wiki](@/wiki/_index.md).

[TOC]

{% note(type="warning") %}
Travail à faire sur l'harmonisation de la FAQ et l'adaptation au public visé (néophyte = faq, avancé = wiki)
{% end %}