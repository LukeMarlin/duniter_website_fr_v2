#!/usr/bin/python3
# in files having extra.EXTERNAL_CONTENT set in their front matter
# this script 
# - fetch external content
# - changes internal links
# - replace existing content

import os
import toml
import requests

# recursively check files inside given folder
def replace_all_in(folder):
    for root, dirs, files in os.walk(folder):
        print(files)
        for name in files:
            filename = os.path.join(root, name)
            replace(root, filename)

# if file has external content, replace it
def replace(root, file):
    print(file)
    # read file parts
    with open(file) as f:
        text = f.read()
        front_matter_text = get_front_matter(text)
        front_matter = toml.loads(front_matter_text)
        if "extra" in front_matter.keys() and "EXTERNAL_CONTENT" in front_matter["extra"].keys():
            url = front_matter["extra"]["EXTERNAL_CONTENT"]
            print("requesting content")
            response = requests.get(url)
            print(response.status_code)
    # modify content to match zola's style markdown
    body = modify(root, response.text)
    # overwrite file
    with open(file, "w") as f:
        f.write(f"""+++{front_matter_text}+++

{body}        
""")

# gets front matter
def get_front_matter(text):
    parts = text.split("+++", maxsplit=2)
    return parts[1]

# alter text body
def modify(root, body):
    # changes internal linking style
    body = body.replace("](./", f"](@/{root}/")
    return body


if __name__ == "__main__":
    os.chdir("content")
    replace_all_in("wiki/documentation/duniter1.9doc")