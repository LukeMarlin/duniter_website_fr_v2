# Site web de Duniter

Ce site est réalisé en Zola et sert de support technique pour présenter et utiliser le logiciel Duniter. Il présente également les concepts de bases d'une monnaie libre et du fonctionnement de la monnaie ğ1.

## Utilisation

Pour modifier le site localement, veuillez d'abord cloner le dépôt :

```
git clone https://git.42l.fr/HugoTrentesaux/zola-duniter
```

La plupart des composants sont directement inclus, mais certains comme Forkawesome (pour les icones) sont disponibles comme sous-module git. Vous pouvez les ajouter avec :

```
git submodule init
git submodule update
```

Après avoir installé [Zola](https://www.getzola.org/documentation/getting-started/installation/), lancez dans le dossier principal :

```
zola serve
```

et cliquez sur le lien [http://127.0.0.1:1111](http://127.0.0.1:1111) pour consulter le site localement (rafraîchi automatiquement en cas de modification).

Pour mettre à jour la documentation automatiquement depuis le gitlab, veuillez exécuter le script dédié (au stade de prototype, peut casser à tout moment) :

```
./scripts/get_external_content.py
```

## Vue d'ensemble

La plupart du contenu du site est situé dans dans le dossier `content` sous forme de fichiers Markdown (texte amélioré) avec des métadonnées en en-tête. Les fichiers statiques (images, documents) sont pour la plupart dans le dossier `static`, une grande partie étant dans le dossier `PELICAN`, issu de la migration du site initial. Le thème est constitué de fichiers html utilisant un langage de template situés dans le dossier `templates`. Enfin, certaines feuilles de style sont rédigées en sass (css amélioré) dans le dossier `sass` alors que d'autres sont écrites en css et situés avec les fichiers statiques.

## Contribuer

Pas besoin d'être à l'aise avec `git`, `zola`, `html`, `css`, `javascript`, `markdown` pour contribuer simplement. Il vous suffira de suivre le [guide de contribution](./doc/README.md).